<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.action.LoginAction"%>
<%@page import="com.javaquarium.action.ListerEspeceAction"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <%@ include file="/jsp/parts/header.jsp"%>
      <title>
         <bean:message key="page.title.lister_especes" />
      </title>
   </head>
   <body>
      <div class="container">
         <%@ include file="/jsp/parts/locale.jsp" %>
         <h1>
            <bean:message key="message.welcome" />
            <span class="grey">
               <bean:write name="<%=SessionVar.USER.toString()%>" property="name" />
            </span>
            <a class="btn btn-info btn-xs" href="<%=Forward.GOTO_MY_PROFILE%>">
               <bean:message key="login.my_profile" />
               <i class="fa fa-user" aria-hidden="true"></i>
            </a>
            <a class="btn btn-basic btn-xs" href="<%=Forward.GOTO_LOGOUT%>">
               <bean:message key="login.logout" />
               <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>
            <a class="btn btn-success btn-xs" href="<%=Forward.GOTO_LIVE_AQUARIUM%>">
               <bean:message key="message.my_live_aquarium" />
               <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>
         </h1>
         <!-- Message boxes for warnings/errors/info -->
         <%@ include file="/jsp/parts/messages.jsp" %>
         <div class="hud">
            <ul class="list-group">
               <li class="list-group-item bg-black">
                  <span class="bold white">
                     <bean:message key="message.my_aquarium" />
                     :
                  </span>
                  <div class="right">
                     <a class="btn btn-success btn-xs" data-toggle="confirmation" 
                        data-btn-ok-label="<bean:message key="message.yes" />" data-btn-ok-icon="glyphicon glyphicon-check"
                        data-btn-ok-class="btn-success"
                        data-btn-cancel-label="<bean:message key="message.no" />" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                        data-btn-cancel-class="btn-danger"
                        data-title="<bean:message key="message.confirmation" />" data-content="<bean:message key="confirmation.save_aquarium" />"
                        data-placement="bottom"
                        href="<%=Forward.GOTO_SAVE_AQUARIUM%>">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        <bean:message key="message.save" />
                     </a>
                     <a class="btn btn-danger btn-xs" data-toggle="confirmation" 
                        data-btn-ok-label="<bean:message key="message.yes" />" data-btn-ok-icon="glyphicon glyphicon-check"
                        data-btn-ok-class="btn-success"
                        data-btn-cancel-label="<bean:message key="message.no" />" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                        data-btn-cancel-class="btn-danger"
                        data-title="<bean:message key="message.confirmation" />" data-content="<bean:message key="confirmation.clear_aquarium" />"
                        data-placement="bottom"
                        href="<%=Forward.GOTO_CLEAR_AQUARIUM%>">
                        <i class="fa fa-eraser" aria-hidden="true"></i>
                        <bean:message key="message.clear" />
                     </a>
                  </div>
               </li>
               <li class="list-group-item green">
                  <bean:message key="message.total_poissons" />
                  <span class="badge">
                     <bean:write name="<%=SessionVar.TOTAL_POISSONS_SAVED.toString()%>" />
                  </span>
               </li>
               <li class="list-group-item orange">
                  <bean:message key="message.total_poissons_not_saved" />
                  <span class="badge">
                     <bean:write name="<%=SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()%>" />
                  </span>
                  <logic:notEqual name="<%=SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()%>" value="0">
                     <i class="fa fa-exclamation-triangle faa-flash animated red" aria-hidden="true"></i>
                  </logic:notEqual>
               </li>
            </ul>
         </div>
         <table class="table table-bordered">
            <thead style="background-color: grey; color: white;">
               <tr>
               	  <th class="center">
                     <i class="fa fa-users" aria-hidden="true"></i>
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.name" />
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.description" />
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.color" />
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.size" />
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.price" />
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.detail" />
                  </th>
                  <th>
                     <bean:message key="liste_poisson.colonne.my_aquarium" />
                  </th>
               </tr>
            </thead>
            <logic:iterate name="<%=SessionVar.MY_AQUARIUM.toString()%>" id="poisson">
               <bean:define id="aquariumEspece" name="poisson" property="value" />
               <bean:define id="nomEspece" name="aquariumEspece" property="espece.espece" />
               <tr>
               	  <th class="center" scope="row">
               	  	<a class="btn btn-warning btn-sm" href="<%=Forward.GOTO_EDIT_POISSON%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />"> <i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                    <a class="btn btn-danger btn-sm" data-toggle="confirmation"
                     	data-btn-ok-label="<bean:message key="message.yes" />" data-btn-ok-icon="glyphicon glyphicon-check"
                        data-btn-ok-class="btn-success"
                        data-btn-cancel-label="<bean:message key="message.no" />" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                        data-btn-cancel-class="btn-danger"
                        data-title="<bean:message key="message.confirmation" />" data-content="<bean:message key="confirmation.remove_poisson" arg0="<%=nomEspece.toString()%>" />"
                        data-placement="bottom"
                    	href="<%=Forward.GOTO_REMOVE_POISSON%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />">
                    	<i class="fa fa-trash" aria-hidden="true"></i>
                    </a> 
                  </th>
                  <th scope="row">
                     <bean:write name='aquariumEspece' property='espece.espece' />
                  </th>
                  <th scope="row">
                     <bean:write name='aquariumEspece' property='espece.description' />
                  </th>
                  <th scope="row">
                     <bean:write name='aquariumEspece' property='espece.couleur' />
                  </th>
                  <th scope="row">
                     <bean:write name='aquariumEspece' property='espece.dimension' />
                  </th>
                  <th scope="row">
                     <bean:write name='aquariumEspece' property='espece.prix' />
                  </th>
                  <th scope="row">
                     <a class="btn btn-primary btn-xs" href="#">
                        <bean:message key="liste_poisson.colonne.detail" />
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                     </a>
                  </th>
                  <th scope="row">
                     <a class="btn btn-success btn-xs" href="<%=Forward.GOTO_AJOUT_AQUARIUM%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />">
                        <bean:message key="liste_poisson.my_aquarium.add" />
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                     </a>
                     <a class="btn btn-danger btn-xs" href="<%=Forward.GOTO_ENLEVE_AQUARIUM%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />">
                        <bean:message key="liste_poisson.my_aquarium.remove" />
                        <i class="fa fa-minus-circle" aria-hidden="true"></i>
                     </a>
                     <div class="right">
                        <bean:message key="liste_poisson.my_aquarium.actual" />
                        : 
                        <span class="green">
                           <bean:write name='aquariumEspece'
                              property='quantitySaved' />
                        </span>
                        <i class="fa fa-database grey"></i>
                        <i class="fa fa-exchange"></i> 
                        <span class="red">
                           <bean:write
                              name='aquariumEspece' property='quantityNotSaved' />
                        </span>
                        <i class="fa fa-exclamation-triangle orange"></i>
                     </div>
                  </th>
               </tr>
            </logic:iterate>
         </table>
         <a class="btn btn-info" href="<%=Forward.GOTO_AJOUT%>">
            <bean:message key="liste_poisson.ajouter_poisson" />
            <i class="fa fa-plus" aria-hidden="true"></i>
         </a>
         <img src="./img/dory-icon.png" class="right" />
      </div>
      <%@ include file="/jsp/parts/footer.jsp" %>
   </body>
</html>