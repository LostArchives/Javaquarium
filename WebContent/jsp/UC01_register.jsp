<%@page import="com.javaquarium.consts.Forward"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <!-- CSS files -->
      <%@ include file="/jsp/parts/header.jsp" %>
      <title>
         <bean:message key="page.title.register" />
      </title>
   </head>
   <body>
      <div class="container">
         <!-- Locale switcher with flags -->
         <%@ include file="/jsp/parts/locale.jsp" %>
         <h1>
            <bean:message key="registerUser.form.title" />
         </h1>
         <img src="./img/turtle-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <!-- Message boxes for warnings/errors/info -->
               <%@ include file="/jsp/parts/messages.jsp" %>
               <!-- Beginning Form -->
               <html:form action="/register" focus="name" styleClass="form-horizontal">
                  <!-- Name -->
                  <div class="form-group">
                     <label for="name" class="control-label col-sm-4">
                        <bean:message
                           key="registerUser.form.name" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="registerForm" property="name" styleClass="form-control" />
                     </div>
                  </div>
                  <!-- User name -->
                  <div class="form-group">
                     <label for="username" class="control-label col-sm-4">
                        <bean:message
                           key="registerUser.form.username" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="registerForm" property="username" styleClass="form-control" />
                     </div>
                  </div>
                  <!-- Password -->
                  <div class="form-group">
                     <label for="password" class="control-label col-sm-4">
                        <bean:message
                           key="registerUser.form.password" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="registerForm" property="password" styleClass="form-control" />
                     </div>
                  </div>
                  <!-- Password again -->
                  <div class="form-group">
                     <label for="password2" class="control-label col-sm-4">
                        <bean:message
                           key="registerUser.form.password2" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="registerForm" property="password2" styleClass="form-control" />
                     </div>
                  </div>
                  <!-- Buttons -->
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                        	<a class="btn btn-info btn-md" href="<%=Forward.GOTO_LOGIN%>">
                  				<bean:message key="message.return" />
               				</a>
                           <html:submit styleClass="btn btn-success col-sm-3">
                              <bean:message key="registerUser.form.validate" />
                           </html:submit>
                           <html:reset styleClass="btn btn-warning col-sm-3">
                              <bean:message key="registerUser.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
                  <!-- End form -->
               </html:form>
            </div>
         </div>
      </div>
      <!-- JS files -->
      <%@ include file="/jsp/parts/footer.jsp" %>
   </body>
</html>