<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <!-- CSS files -->
      <%@ include file="/jsp/parts/header.jsp" %>
      <title>
         <bean:message key="page.title.login" />
      </title>
   </head>
   <body>
      <div class="container">
         <!-- Locale switcher with flags -->
         <%@ include file="/jsp/parts/locale.jsp" %>
         <h1>
            <bean:message key="login.form.title" />
         </h1>
         <img src="./img/nemo-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <!-- Message boxes for errors/warnings/info... -->
               <%@ include file="/jsp/parts/messages.jsp" %>
               <!-- Beginning Form -->
               <html:form action="/login" focus="username" styleClass="form-horizontal">
                  <!-- User name -->
                  <div class="form-group">
                     <label for="login" class="control-label col-sm-4">
                        <bean:message
                           key="login.form.user" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="loginForm" property="username" styleClass="form-control" />
                     </div>
                  </div>
                  <!-- Password -->
                  <div class="form-group">
                     <label for="password" class="control-label col-sm-4">
                        <bean:message
                           key="login.form.password" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="loginForm" property="password" styleClass="form-control" />
                     </div>
                     <a href="<%=Forward.GOTO_REGISTER%>" class="right">
                        <bean:message key="login.form.not_yet_user" />
                     </a>
                  </div>
                  <!-- Buttons -->
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                           <html:submit styleClass="btn btn-primary col-sm-3">
                              <bean:message key="login.form.submit" />
                           </html:submit>
                           <html:reset styleClass='btn btn-primary col-sm-3'>
                              <bean:message key="login.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
                  <!-- End Form -->
               </html:form>
            </div>
         </div>
      </div>
      <!-- JS files -->
      <%@ include file="/jsp/parts/footer.jsp" %>
   </body>
</html>