<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic"
   prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <%@ include file="/jsp/parts/header.jsp" %>
      <link rel="stylesheet" type="text/css" href="./css/aquarium.css">
      <title>
         <bean:message key="page.title.live_aquarium" />
      </title>
   </head>
   <body>
      <nav class="navbar navbar-inverse">
         <div class="navbar-header">
            <div class="navbar-brand">
               <a class="navbar-center navbar-link" href="#">
                  <bean:message key="message.aquarium_of" />
                  : 
                  <bean:write name="<%=SessionVar.USER.toString()%>" property="name" />
                  | 
                  <bean:message key="message.total_poissons" />
                  : 
                  <bean:write name="<%=SessionVar.TOTAL_POISSONS_SAVED.toString()%>" />
               </a>
               <a class="btn btn-default btn-md navbar-right navbar-link" href="<%=Forward.GOTO_LISTE_ESPECE%>">
                  <bean:message key="message.return_my_aquarium" />
                  <i class="fa fa-user" aria-hidden="true"></i>
               </a>
            </div>
         </div>
      </nav>
      <div class="bubbles">
         <div class="bubble-container" style="left: 15%; animation-duration: 2.15232s; animation-delay: 12s;">
            <div class="bubble" style="width: 3px; height: 3px;"></div>
         </div>
         <div class="bubble-container" style="left: 3%; animation-duration: 2.51255s; animation-delay: 10s;">
            <div class="bubble" style="width: 10px; height: 10px;"></div>
         </div>
         <div class="bubble-container" style="left: 56%; animation-duration: 2.73306s; animation-delay: 4s;">
            <div class="bubble" style="width: 4px; height: 4px;"></div>
         </div>
         <div class="bubble-container" style="left: 78%; animation-duration: 3.01288s; animation-delay: 11s;">
            <div class="bubble" style="width: 7px; height: 7px;"></div>
         </div>
         <div class="bubble-container" style="left: 66%; animation-duration: 3.12107s; animation-delay: 14s;">
            <div class="bubble" style="width: 4px; height: 4px;"></div>
         </div>
         <div class="bubble-container" style="left: 64%; animation-duration: 3.05645s; animation-delay: 9s;">
            <div class="bubble" style="width: 10px; height: 10px;"></div>
         </div>
         <div class="bubble-container" style="left: 21%; animation-duration: 3.04905s; animation-delay: 4s;">
            <div class="bubble" style="width: 5px; height: 5px;"></div>
         </div>
         <div class="bubble-container" style="left: 4%; animation-duration: 1.81146s; animation-delay: 0s;">
            <div class="bubble" style="width: 10px; height: 10px;"></div>
         </div>
         <div class="bubble-container" style="left: 19%; animation-duration: 2.58068s; animation-delay: 8s;">
            <div class="bubble" style="width: 7px; height: 7px;"></div>
         </div>
         <div class="bubble-container" style="left: 26%; animation-duration: 1.82973s; animation-delay: 10s;">
            <div class="bubble" style="width: 5px; height: 5px;"></div>
         </div>
         <div class="bubble-container" style="left: 99%; animation-duration: 2.35876s; animation-delay: 5s;">
            <div class="bubble" style="width: 11px; height: 11px;"></div>
         </div>
         <div class="bubble-container" style="left: 80%; animation-duration: 2.27963s; animation-delay: 6s;">
            <div class="bubble" style="width: 9px; height: 9px;"></div>
         </div>
         <div class="bubble-container" style="left: 87%; animation-duration: 3.05419s; animation-delay: 12s;">
            <div class="bubble" style="width: 11px; height: 11px;"></div>
         </div>
         <div class="bubble-container" style="left: 46%; animation-duration: 2.93312s; animation-delay: 3s;">
            <div class="bubble" style="width: 4px; height: 4px;"></div>
         </div>
         <div class="bubble-container" style="left: 11%; animation-duration: 1.68118s; animation-delay: 3s;">
            <div class="bubble" style="width: 5px; height: 5px;"></div>
         </div>
         <div class="bubble-container" style="left: 76%; animation-duration: 3.01482s; animation-delay: 6s;">
            <div class="bubble" style="width: 9px; height: 9px;"></div>
         </div>
         <div class="bubble-container" style="left: 7%; animation-duration: 3.12305s; animation-delay: 8s;">
            <div class="bubble" style="width: 11px; height: 11px;"></div>
         </div>
         <div class="bubble-container" style="left: 52%; animation-duration: 1.93067s; animation-delay: 8s;">
            <div class="bubble" style="width: 8px; height: 8px;"></div>
         </div>
         <div class="bubble-container" style="left: 10%; animation-duration: 2.57897s; animation-delay: 3s;">
            <div class="bubble" style="width: 9px; height: 9px;"></div>
         </div>
         <div class="bubble-container" style="left: 74%; animation-duration: 2.86057s; animation-delay: 15s;">
            <div class="bubble" style="width: 8px; height: 8px;"></div>
         </div>
         <div class="bubble-container" style="left: 51%; animation-duration: 3.07515s; animation-delay: 11s;">
            <div class="bubble" style="width: 9px; height: 9px;"></div>
         </div>
         <div class="bubble-container" style="left: 14%; animation-duration: 1.81939s; animation-delay: 6s;">
            <div class="bubble" style="width: 8px; height: 8px;"></div>
         </div>
         <div class="bubble-container" style="left: 76%; animation-duration: 1.5901s; animation-delay: 11s;">
            <div class="bubble" style="width: 7px; height: 7px;"></div>
         </div>
         <div class="bubble-container" style="left: 22%; animation-duration: 2.40886s; animation-delay: 1s;">
            <div class="bubble" style="width: 4px; height: 4px;"></div>
         </div>
         <div class="bubble-container" style="left: 13%; animation-duration: 1.69251s; animation-delay: 7s;">
            <div class="bubble" style="width: 7px; height: 7px;"></div>
         </div>
         <div class="bubble-container" style="left: 5%; animation-duration: 3.03535s; animation-delay: 0s;">
            <div class="bubble" style="width: 6px; height: 6px;"></div>
         </div>
         <div class="bubble-container" style="left: 63%; animation-duration: 2.27295s; animation-delay: 14s;">
            <div class="bubble" style="width: 5px; height: 5px;"></div>
         </div>
         <div class="bubble-container" style="left: 29%; animation-duration: 1.64359s; animation-delay: 11s;">
            <div class="bubble" style="width: 11px; height: 11px;"></div>
         </div>
         <div class="bubble-container" style="left: 45%; animation-duration: 2.77238s; animation-delay: 11s;">
            <div class="bubble" style="width: 9px; height: 9px;"></div>
         </div>
         <div class="bubble-container" style="left: 97%; animation-duration: 3.39959s; animation-delay: 15s;">
            <div class="bubble" style="width: 4px; height: 4px;"></div>
         </div>
      </div>
      <div class="ground"></div>
      <div class="rock_1"></div>
      <div class="rock_2"></div>
      <div class="rock_3"></div>
      <div class="rock_4"></div>
      <div class="rock_5"></div>
      <div class="rock_6"></div>
      <div class="rock_7"></div>
      <div class="plant_1_wrap">
         <div class="plant_1"></div>
         <div class="plant_2"></div>
         <div class="plant_3"></div>
      </div>
      <div class="plant_2_wrap">
         <div class="plant_4"></div>
         <div class="plant_5"></div>
      </div>
      <logic:iterate name="<%=SessionVar.MY_LIVE_AQUARIUM.toString()%>" id="poisson" indexId="index">
         <bean:define id="nomEspece" name="poisson" property="espece" />
         <div class="fish" id="fish_<bean:write name='index'/>"><img id="img-fish_<bean:write name='index'/>" data-toggle="tooltip" title="<bean:write name='nomEspece'/>"></div>
      </logic:iterate>
      <%@ include file="/jsp/parts/footer.jsp" %>
      <script src="./js/aquarium/bubbles.js"></script>
      <script src="./js/aquarium/jquery.keyframes.min.js"></script>
      <script src="./js/aquarium/prefixfree.min.js"></script>
      <script src="./js/aquarium/aquarium.js"></script>
   </body>
</html>