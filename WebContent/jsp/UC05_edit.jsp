<%@page import="com.javaquarium.consts.SessionVar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<bean:define id="poissonCode" name="<%=SessionVar.VAR_TO_EDIT.toString()%>" property="code"/>
<bean:define id="poissonEspece" name="<%=SessionVar.VAR_TO_EDIT.toString()%>" property="espece"/>
<bean:define id="poissonDescription" name="<%=SessionVar.VAR_TO_EDIT.toString()%>" property="description"/>
<bean:define id="poissonCouleur" name="<%=SessionVar.VAR_TO_EDIT.toString()%>" property="couleur"/>
<bean:define id="poissonPrix" name="<%=SessionVar.VAR_TO_EDIT.toString()%>" property="prix"/>
<bean:define id="poissonDimensions" name="<%=SessionVar.VAR_TO_EDIT.toString()%>" property="dimension"/>

<head>
      <%@ include file="/jsp/parts/header.jsp" %>
      <title>
         <bean:message key="page.title.edit_poisson"/>
         <%=poissonEspece.toString()%>
      </title>
   </head>

<body>
      <div class="container">
         <%@ include file="/jsp/parts/locale.jsp" %>
         <h1>
            <bean:message key="editPoisson.form.title" />
            <%=poissonEspece.toString()%>
         </h1>
         <img src="./img/destiny-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <%@ include file="/jsp/parts/messages.jsp" %>
               <html:form action="/editPoisson" styleClass="form-horizontal">
               		<html:hidden name="editPoissonForm" property="code" value="<%=poissonCode.toString()%>" />
                  <div class="form-group">
                     <label for="espece" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.name" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="espece" styleClass="form-control" value="<%=poissonEspece.toString()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="description" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.description" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:textarea name="editPoissonForm" property="description" styleClass="form-control" value="<%=poissonDescription.toString()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="couleur" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.couleur" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="couleur" styleClass="form-control" value="<%=poissonCouleur.toString()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="prix" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.prix" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="prix" styleClass="form-control" value="<%=poissonPrix.toString()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="dimension" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.dimensions" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="dimension" styleClass="form-control" value="<%=poissonDimensions.toString()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                           <a class="btn btn-info btn-md" href="<%=Forward.GOTO_LISTE_ESPECE%>">
                              <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                              <bean:message key="message.return_my_aquarium" />
                           </a>
                           <html:submit styleClass="btn btn-success col-sm-3">
                              <bean:message key="editPoisson.form.validate" />
                           </html:submit>
                           <html:reset styleClass="btn btn-warning col-sm-3">
                              <bean:message key="editPoisson.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
               </html:form>
            </div>
         </div>
      </div>
      <%@ include file="/jsp/parts/footer.jsp" %>
   </body>
</html>