$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle=confirmation]').confirmation({
    	  rootSelector: '[data-toggle=confirmation]',
    	  // other options
    	});
    $(".checkbox").attr("checked",false);
    $('.switch').attr("disabled",true);
    $(".checkbox").click(function () {
        $('.switch').attr("disabled", !$(this).is(":checked"));
     });
});
