Javaquarium
===================


**Javaquarium** is a J2EE project whose objective is to use **Struts**, **Hibernate** and **Spring** in the context of a virtual aquarium management

----------

Introduction
-------------

This project was developed by using a **3-tier architecture** with **DAO,Services,Actions, VO and DO beans**.
The objective is to create pages and features in order to be able to **manage fishes (add,update and remove)**,
**users (sign in and sign up)** and their own **virtual aquarium** containing fishes where they can add or remove fishes as they want

> **Note:**

> - The version of Struts which is used is **version 1**
> - The application was developed by using the Spring IoC (old way of doing with .xml file)
> - The project is **ready to use** (all is already included in the project, you just need your **Tomcat server** and **import the project into Eclipse**

------------

Java Structure
---------------

The java part of the project is divided into **multiple packages** such as :


#### <i class="icon-file"></i> Action

This is the package where you will find **all Struts actions**

#### <i class="icon-folder-open"></i> Beans

This is the package where you will find all Java beans. They are divided into two types :
	**Beans.Data** : All beans (VO and DO) who represents data objects
	**Beans.Web** : All beans who represents web objects such as **forms**

#### <i class="icon-pencil"></i> Business

This is the package where you will find all your business logic with your **Services**, their interfaces and mappers to convert your beans between VO and DO format

#### <i class="icon-star"></i> DAO

This is the package where you will find your **DAO** classes and their interfaces

#### <i class="icon-play"></i> Exceptions

This is the package where you will find all classes who manage **the exception system** between the diferent layers of your application (**DAO -> Services -> Action -> JSP**)

#### <i class="icon-hdd"></i> Consts

This is the package where you will find all final classes and enums which contain the constant variables used in your classes and JSP files.
With this, you have a good flexibility to modify the application or implemented new features easily.

#### <i class="icon-user"></i> Util

This is the package where you will find all utilities such as catalogues for DAOs and Services or for Hibernate. 

> **Note:**

> - At the root of the javaquarium package, yoiu will find 3 files :
> - **The 2 ApplicationResources files (one for english and another one for french)**
> - **The hibernatecfg.xml file for Hibernate configuration**

----------


Web structure
-------------------

The web content of this project is divided into multiple folders :
>- CSS, JS and IMG folders are classic web folders

#### <i class="icon-refresh"></i> JSP

The jsp folder contain all jsp files and also a subfolder called **parts**

#### <i class="icon-refresh"></i> JSP/Parts

The folder "parts" contain jsp files which can can reused by including them into other jsp
You will find header, footer and also other very useful jsp files...

#### <i class="icon-refresh"></i> WEB-INF

This folder contains 3 important files which are :

[1] -**The ApplicationContext file for Spring and IoC**
[2] - **The struts-config file for Struts**
[3] - **The web xml to change the setup of your application**

