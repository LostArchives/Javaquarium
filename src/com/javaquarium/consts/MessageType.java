package com.javaquarium.consts;

/**
 * Enum to define the different type of messages It is used to filter messages
 * and display them differently in jsp (check messages.jsp in 'parts' folder)
 * 
 * @author Valentin
 *
 */
public enum MessageType {

	INFO, ERROR, SUCCESS, WARNING

}
