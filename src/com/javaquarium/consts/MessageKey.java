package com.javaquarium.consts;

/**
 * A final class to manage useful message keys from ApplicationResources (useful
 * for errors and message displaying)
 * 
 * @author Valentin
 *
 */
public final class MessageKey {

	public static final String ERROR_DEFAULT = "error.default_message";

	public static final String ERROR_DATABASE_CONNECTION = "error.database.connection";

	public static final String ERROR_DUPLICATE_USER = "error.register.user.already_exist";

	public static final String ERROR_DUPLICATE_POISSON = "error.add.poisson.already_exist";

	public static final String ERROR_GET_NONUNIQUE = "error.get.expected_unique_result";

	public static final String ERROR_INVALID_ID = "error.invalid_id";

	public static final String ERROR_NOEXIST_POISSON = "error.poisson.not_exist";

	public static final String WARNING_NOT_LOGGED = "message.access_not_logged";

	public static final String WARNING_BAD_LOGIN_PASSWORD = "error.login.bad_login_password";

	public static final String SUCCESS_ADD_POISSON = "ajoutPoisson.form.success_message";

	public static final String SUCCESS_EDIT_POISSON = "editPoisson.form.success_message";

	public static final String SUCCESS_REMOVE_POISSON = "removePoisson.success_message";

	public static final String SUCCESS_ADD_USER = "registerUser.form.success_message";

	public static final String SUCCESS_EDIT_USER = "editProfile.form.success_message";

	public static final String SUCCESS_SAVE_AQUARIUM = "saveAquarium.success_message";

	public static final String SUCCESS_CLEAR_AQUARIUM = "clearAquarium.success_message";

	public static final String SUCCESS_LOGOUT = "logout.success_message";

	/**
	 * DON'T CALL ME !!!
	 */
	private MessageKey() {
	}

}
