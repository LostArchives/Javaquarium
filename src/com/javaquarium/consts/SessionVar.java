package com.javaquarium.consts;

/**
 * Enum to define the different session variables used in this application
 * 
 * @author Valentin
 *
 */
public enum SessionVar {

	USER, MY_AQUARIUM, MY_LIVE_AQUARIUM, TOTAL_POISSONS_SAVED, TOTAL_POISSONS_NOT_SAVED, VAR_TO_EDIT

}
