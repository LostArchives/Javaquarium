package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.UserVO;
import com.javaquarium.beans.web.LoginVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Classic Action to allow the user to sign in
 * 
 * @author Valentin
 *
 */
public class LoginAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();
		final LoginVO loginForm = (LoginVO) form;
		final UserVO user;

		try {
			// try to find a user with this username and password
			user = this.catalogueService.getUserService().getOne(loginForm.getUsername(), loginForm.getPassword());
		} catch (final ServiceException se) {
			se.printError();
			messages.add(MessageType.ERROR.toString(), se.getError(null));
			saveMessages(req, messages);
			// return to the page with the login form
			return mapping.getInputForward();
		}

		// if username and password are correct
		if (user != null) {
			// store the user as session variable
			session.setAttribute(SessionVar.USER.toString(), user);
			return mapping.findForward(Forward.SUCCESS);
		} else {
			messages.add(MessageType.WARNING.toString(), new ActionMessage(MessageKey.WARNING_BAD_LOGIN_PASSWORD));
			saveMessages(req, messages);
			// return to the page with the login form
			return mapping.getInputForward();

		}

	}

}
