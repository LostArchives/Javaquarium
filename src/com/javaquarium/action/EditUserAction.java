package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.UserVO;
import com.javaquarium.beans.web.ProfileVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

public class EditUserAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final HttpSession session = request.getSession();
		final ProfileVO user = (ProfileVO) form;
		final UserVO currentUser = (UserVO) session.getAttribute(SessionVar.USER.toString());
		final ActionMessages messages = new ActionMessages();

		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			try {
				this.catalogueService.getUserService().update(user);
				updateSessionUser(currentUser, user);
				messages.add(MessageType.SUCCESS.toString(), new ActionMessage(MessageKey.SUCCESS_EDIT_USER));
			} catch (final ServiceException se) {
				se.printError();
				messages.add(MessageType.WARNING.toString(), se.getError(new String[] { user.getUsername() }));
				saveMessages(request, messages);
				return mapping.getInputForward();
			}

		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(request, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

		saveMessages(request, messages);
		return mapping.findForward(Forward.SUCCESS);
	}

	/**
	 * Method to update the session variable
	 * 
	 * @param currentUser
	 *            The current user session variable
	 * @param updatedUser
	 *            The updated profile of the current user
	 */
	private void updateSessionUser(final UserVO currentUser, final ProfileVO updatedUser) {
		currentUser.setName(updatedUser.getName());
	}

}
