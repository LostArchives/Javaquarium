package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

public class RemovePoissonAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final ActionMessages messages = new ActionMessages();
		final HttpSession session = req.getSession();

		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			if (req.getParameter(Param.ESPECE_ID) != null) {
				final int especeId = Integer.parseInt(req.getParameter(Param.ESPECE_ID));
				try {
					this.catalogueService.getPoissonService().removeOne(especeId);
					session.removeAttribute(SessionVar.MY_AQUARIUM.toString());
					messages.add(MessageType.SUCCESS.toString(), new ActionMessage(MessageKey.SUCCESS_REMOVE_POISSON));
					saveMessages(req, messages);
				} catch (final ServiceException se) {
					se.printError();
					messages.add(MessageType.WARNING.toString(),
							new ActionMessage(se.getMessageKey(), new String[] { especeId + "" }));
					saveMessages(req, messages);
				}
			}

		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

		return mapping.findForward(Forward.SUCCESS);

	}

}
