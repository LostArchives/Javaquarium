package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;

/**
 * Classic action to sign out the user when he is connected
 * 
 * @author Valentin
 *
 */
public class LogoutAction extends Action {

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();

		// Delete all session variables
		session.removeAttribute(SessionVar.USER.toString());
		session.removeAttribute(SessionVar.MY_AQUARIUM.toString());
		session.removeAttribute(SessionVar.TOTAL_POISSONS_SAVED.toString());
		session.removeAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString());
		session.removeAttribute(SessionVar.VAR_TO_EDIT.toString());

		messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.SUCCESS_LOGOUT));
		saveMessages(req, messages);

		return mapping.findForward(Forward.SUCCESS);

	}

}
