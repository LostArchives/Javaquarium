package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

public class EditPoissonAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final HttpSession session = request.getSession();
		final PoissonVO toEdit = (PoissonVO) form;
		final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			// if a VO object needs to be edited (avoid direct url access)
			if (session.getAttribute(SessionVar.VAR_TO_EDIT.toString()) != null) {
				try {
					this.catalogueService.getPoissonService().update(toEdit);
					// to force the update of the session variable after forward
					session.removeAttribute(SessionVar.MY_AQUARIUM.toString());
					messages.add(MessageType.SUCCESS.toString(),
							new ActionMessage(MessageKey.SUCCESS_EDIT_POISSON, new String[] { toEdit.getEspece() }));
					saveMessages(request, messages);
				} catch (final ServiceException se) {
					se.printError();
				}
				return mapping.findForward(Forward.SUCCESS);
			} else {
				return mapping.findForward(Forward.FAIL);
			}
		} else {
			messages.add(MessageType.WARNING.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(request, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
