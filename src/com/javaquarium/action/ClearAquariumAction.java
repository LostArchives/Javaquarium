package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.UserVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Action to clear the aquarium content
 * 
 * @author Valentin
 *
 */
public class ClearAquariumAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final ActionMessages messages = new ActionMessages();
		final UserVO user = (UserVO) req.getSession().getAttribute(SessionVar.USER.toString());

		if (user != null) {

			try {
				this.catalogueService.getAquariumPoissonService().removeAllAquariumPoisson(user.getId());
				req.getSession().removeAttribute(SessionVar.MY_AQUARIUM.toString());
				req.getSession().removeAttribute(SessionVar.TOTAL_POISSONS_SAVED.toString());
				req.getSession().removeAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString());
			} catch (final ServiceException se) {
				se.printError();
				messages.add(MessageType.ERROR.toString(), se.getError(null));
				saveMessages(req, messages);
				return mapping.getInputForward();
			}

		}

		return mapping.findForward(Forward.SUCCESS);
	}

}
