package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.javaquarium.consts.Forward;
import com.javaquarium.consts.SessionVar;

/**
 * Action to redirect to different pages which requires to be logged
 * 
 * @author Valentin
 *
 */
public class GotoAction extends Action {

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		//final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			return mapping.findForward(Forward.SUCCESS);
		} else {
			//messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			//saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

	}

}
