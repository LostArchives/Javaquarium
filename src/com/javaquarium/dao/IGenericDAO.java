package com.javaquarium.dao;

import java.util.List;

public interface IGenericDAO<E> {

	List<E> getAll();

	E getOne(int id);

	void add(E object);

	void update(E object);

	void delete(E object);

}
