package com.javaquarium.dao;

import java.util.List;

import org.hibernate.JDBCException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;

import com.javaquarium.beans.data.PoissonDO;
import com.javaquarium.consts.ExceptionCode;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.exception.DAOException;
import com.javaquarium.util.HibernateUtils;

/**
 * Classic DAO to manage the PoissonDO and interract with database
 * 
 * @author Valentin
 *
 */
public class PoissonDAO implements IPoissonDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<PoissonDO> getAll() throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session.createQuery("from PoissonDO");
		try {
			final List<PoissonDO> list = query.list();
			return list;
		} catch (final JDBCException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}

	}

	@Override
	public PoissonDO getOneById(final int especeId) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session.createQuery("from PoissonDO where id = :id");
		query.setParameter("id", especeId);
		try {
			final PoissonDO poissonDO = (PoissonDO) query.uniqueResult();
			return poissonDO;
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final NonUniqueResultException nre) {
			throw new DAOException(ExceptionCode.ERROR_GET_NONUNIQUE, MessageKey.ERROR_GET_NONUNIQUE,
					nre.getMessage().toString());
		} finally {
			session.close();
		}

	}

	@Override
	public PoissonDO getOneByName(final String name) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session.createQuery("from PoissonDO where nom= :name");
		query.setParameter("name", name);
		try {
			final PoissonDO poissonDO = (PoissonDO) query.uniqueResult();
			return poissonDO;
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final NonUniqueResultException nre) {
			throw new DAOException(ExceptionCode.ERROR_GET_NONUNIQUE, MessageKey.ERROR_GET_NONUNIQUE,
					nre.getMessage().toString());
		} finally {
			session.close();
		}

	}

	@Override
	public void addOne(final PoissonDO pdo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.save(pdo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final ConstraintViolationException cve) {
			throw new DAOException(ExceptionCode.ERROR_DUPLICATE_POISSON, MessageKey.ERROR_DUPLICATE_POISSON, pdo);
		} finally {
			session.close();
		}
	}

	@Override
	public void update(final PoissonDO pdo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.update(pdo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final ConstraintViolationException cve) {
			throw new DAOException(ExceptionCode.ERROR_DUPLICATE_POISSON, MessageKey.ERROR_DUPLICATE_POISSON, pdo);
		} finally {
			session.close();
		}
	}

	@Override
	public void removeOne(final PoissonDO pdo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.delete(pdo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} finally {
			session.close();
		}
	}

	@Override
	public void removeOne(final int especeId) throws DAOException {
		final PoissonDO pdo = getOneById(especeId);

		if (pdo == null) {
			throw new DAOException(ExceptionCode.ERROR_ELEMENT_NOT_EXIST, MessageKey.ERROR_NOEXIST_POISSON,
					"ID : " + especeId);
		} else {
			final Session session = HibernateUtils.getSession();
			final Transaction tx = session.beginTransaction();
			try {
				session.delete(pdo);
				tx.commit();
			} catch (final JDBCConnectionException ce) {
				throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
			} finally {
				session.close();
			}
		}

	}

}
