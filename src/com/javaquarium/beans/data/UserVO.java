package com.javaquarium.beans.data;

public class UserVO {

	private int id;

	private String name;

	private String username;

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Type : " + getClass().getName().toString() + "\n");
		sb.append("Id : " + this.id + "\n");
		sb.append("Nom : " + this.name + "\n");
		sb.append("Nom d'utilisateur : " + this.username + "\n");

		return sb.toString();
	}

}
