package com.javaquarium.beans.data;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.javaquarium.consts.MessageType;

/**
 * JavaBean Class to represent a PoissonVO
 * 
 * @author Valentin
 *
 */
public class PoissonVO extends ActionForm {

	private static final long serialVersionUID = -984799545553767687L;
	private Integer code;
	private String espece;
	private String description;
	private String couleur;
	private String dimension;
	private String prix;

	public static final String REGEX_DIMENSION = "([0-9]+[.][0-9]{0,2})[ ]{0,1}x[ ]{0,1}([0-9]+[.][0-9]{0,2})";

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(final Integer code) {
		this.code = code;
	}

	/**
	 * @return the espece
	 */
	public String getEspece() {
		return this.espece;
	}

	/**
	 * @param espece
	 *            the espece to set
	 */
	public void setEspece(final String espece) {
		this.espece = espece;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * @return the couleur
	 */
	public String getCouleur() {
		return this.couleur;
	}

	/**
	 * @param couleur
	 *            the couleur to set
	 */
	public void setCouleur(final String couleur) {
		this.couleur = couleur;
	}

	/**
	 * @return the dimension
	 */
	public String getDimension() {
		return this.dimension;
	}

	/**
	 * @param dimension
	 *            the dimension to set
	 */
	public void setDimension(final String dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the prix
	 */
	public String getPrix() {
		return this.prix;
	}

	/**
	 * @param prix
	 *            the prix to set
	 */
	public void setPrix(final String prix) {
		this.prix = prix;
	}

	@Override
	public void reset(final ActionMapping mapping, final HttpServletRequest request) {
		this.espece = "";
		this.description = "";
		this.couleur = "";
		this.prix = "";
		this.dimension = "";
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest req) {
		final ActionErrors errors = new ActionErrors();

		if (this.espece.length() == 0) {
			errors.add(MessageType.ERROR.toString(), new ActionMessage("error.add.poisson.no_name"));
		}

		try {
			Integer.parseInt(this.prix);
		} catch (final NumberFormatException e) {
			errors.add(MessageType.ERROR.toString(), new ActionMessage("error.add.poisson.price_not_integer"));
		}

		if (!this.dimension.matches(REGEX_DIMENSION)) {
			errors.add(MessageType.ERROR.toString(), new ActionMessage("error.add.poisson.bad_dimension_format"));
		}

		return errors;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Type : " + getClass().getName().toString() + "\n");
		sb.append("Id : " + this.code + "\n");
		sb.append("Nom : " + this.espece + "\n");
		sb.append("Couleur : " + this.couleur + "\n");
		sb.append("Description : " + this.description + "\n");
		sb.append("Prix : " + this.prix + "\n");
		sb.append("Dimensions : " + this.dimension + "\n");
		return sb.toString();
	}

}
