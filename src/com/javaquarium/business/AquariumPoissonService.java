package com.javaquarium.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.javaquarium.beans.data.AquariumPoissonDO;
import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.beans.data.UserDO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.exception.DAOException;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueDAO;
import com.javaquarium.util.CatalogueService;

/**
 * Classic service to manage aquariumPoisson and the content of the aquarium
 * 
 * @author Valentin
 *
 */
public class AquariumPoissonService implements IAquariumPoissonService {

	/**
	 * Instance of the catalogueService
	 */
	private CatalogueService catalogueService;

	/**
	 * Instance of the catalogueDAO
	 */
	private CatalogueDAO catalogueDAO;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	/**
	 * @param catalogueDAO
	 *            the catalogueDAO to set
	 */
	public void setCatalogueDAO(final CatalogueDAO catalogueDAO) {
		this.catalogueDAO = catalogueDAO;
	}

	@Override
	public Map<Integer, AquariumPoissonVO> getAllAquariumPoisson(final int userId) throws ServiceException {
		final Map<Integer, AquariumPoissonVO> aquarium = new HashMap<Integer, AquariumPoissonVO>();

		try {
			final List<PoissonVO> listeEspeces = this.catalogueService.getPoissonService().getAll();
			final UserVO user = this.catalogueService.getUserService().getOneById(userId);
			for (final PoissonVO espece : listeEspeces) {
				final AquariumPoissonVO aquariumPoissonVO = getAquariumPoisson(user, espece);
				aquarium.put(espece.getCode(), aquariumPoissonVO);
			}
			return aquarium;
		} catch (final ServiceException se) {
			se.printError();
			throw new ServiceException(se.getCode(), se.getMessageKey(), se.getCauseObject());
		}
	}

	@Override
	public AquariumPoissonVO getAquariumPoisson(final int userId, final int especeId) throws ServiceException {

		try {
			final AquariumPoissonDO aPdo = this.catalogueDAO.getAquariumPoissonDAO().getAquariumPoisson(userId,
					especeId);
			if (aPdo == null) {

				final AquariumPoissonVO aquariumPoissonVO = new AquariumPoissonVO();
				aquariumPoissonVO.setId(especeId);
				aquariumPoissonVO.setQuantitySaved(0);
				aquariumPoissonVO.setQuantityNotSaved(0);
				aquariumPoissonVO.setEspece(this.catalogueService.getPoissonService().getOneById(especeId));

				return aquariumPoissonVO;

			} else {
				return getVOfromDO(aPdo);
			}
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		} catch (final ServiceException se) {
			se.printError();
			throw new ServiceException(se.getCode(), se.getMessageKey(), se.getCauseObject());
		}

	}

	@Override
	public AquariumPoissonVO getAquariumPoisson(final UserVO user, final PoissonVO espece) throws ServiceException {

		try {
			final AquariumPoissonDO aPdo = this.catalogueDAO.getAquariumPoissonDAO().getAquariumPoisson(user.getId(),
					espece.getCode());
			if (aPdo == null) {
				final AquariumPoissonVO aquariumPoissonVO = new AquariumPoissonVO();
				aquariumPoissonVO.setId(espece.getCode());
				aquariumPoissonVO.setQuantitySaved(0);
				aquariumPoissonVO.setQuantityNotSaved(0);
				aquariumPoissonVO.setEspece(espece);
				return aquariumPoissonVO;
			} else
				return getVOfromDO(aPdo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public void addNewAquariumPoisson(final int userId, final int especeId, final int quantity)
			throws ServiceException {

		final AquariumPoissonDO aPdo = new AquariumPoissonDO();
		try {
			aPdo.setUser(this.catalogueDAO.getUserDAO().getOneById(userId));
			aPdo.setEspece(this.catalogueDAO.getPoissonDAO().getOneById(especeId));
			aPdo.setQuantity(quantity);
			this.catalogueDAO.getAquariumPoissonDAO().addNewAquariumPoisson(aPdo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public void addNewAquariumPoisson(final UserVO user, final PoissonVO espece, final int quantity)
			throws ServiceException {

		final AquariumPoissonDO aPdo = new AquariumPoissonDO();

		aPdo.setUser(this.catalogueService.getUserService().getDOfromVO(user));
		aPdo.setEspece(this.catalogueService.getPoissonService().getDOfromVO(espece));
		aPdo.setQuantity(quantity);

		try {
			this.catalogueDAO.getAquariumPoissonDAO().addNewAquariumPoisson(aPdo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe.getCode(), daoe.getMessageKey(), aPdo);
		}

	}

	@Override
	public void removeAquariumPoisson(final AquariumPoissonDO aPdo) throws ServiceException {
		try {
			this.catalogueDAO.getAquariumPoissonDAO().removeAquariumPoisson(aPdo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe.getCode(), daoe.getMessageKey(), aPdo);
		}

	}

	@Override
	public void removeAllAquariumPoisson(final int userId) throws ServiceException {
		try {
			this.catalogueDAO.getAquariumPoissonDAO().removeAllAquariumPoisson(userId);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public void updateAquariumPoisson(final AquariumPoissonDO aPdo) throws ServiceException {
		try {
			this.catalogueDAO.getAquariumPoissonDAO().updateAquariumPoisson(aPdo);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe.getCode(), daoe.getMessageKey(), aPdo);
		}

	}

	@Override
	public int getTotalAquariumPoisson(final int userId) throws ServiceException {
		try {
			return this.catalogueDAO.getAquariumPoissonDAO().getTotalAquariumPoissons(userId);
		} catch (final DAOException daoe) {
			daoe.printError();
			throw new ServiceException(daoe);
		}

	}

	@Override
	public AquariumPoissonVO getVOfromDO(final AquariumPoissonDO apDO) {
		final AquariumPoissonVO aquariumPoissonVO = new AquariumPoissonVO();

		aquariumPoissonVO.setId(apDO.getId());
		aquariumPoissonVO.setQuantitySaved(apDO.getQuantity());
		aquariumPoissonVO.setQuantityNotSaved(0);
		aquariumPoissonVO.setEspece(this.catalogueService.getPoissonService().getVOfromDO(apDO.getEspece()));
		return aquariumPoissonVO;

	}

	@Override
	public AquariumPoissonDO getDOfromVO(final AquariumPoissonVO apVO, final UserVO uVo) {
		final AquariumPoissonDO apDO = new AquariumPoissonDO();
		apDO.setId(apVO.getId());
		apDO.setQuantity(apVO.getQuantitySaved() + apVO.getQuantityNotSaved());
		apDO.setEspece(this.catalogueService.getPoissonService().getDOfromVO(apVO.getEspece()));

		final UserDO user = this.catalogueService.getUserService().getDOfromVO(uVo);

		apDO.setUser(user);
		return apDO;

	}

}
