package com.javaquarium.business;

import com.javaquarium.beans.data.UserDO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.beans.web.ProfileVO;
import com.javaquarium.beans.web.RegisterVO;
import com.javaquarium.exception.ServiceException;

/**
 * Classic service interface related to users
 * 
 * @author Valentin
 *
 */
public interface IUserService {

	/**
	 * Method to get a UserVO based on a combination of a username and a password
	 * 
	 * @param username
	 *            The username searched
	 * @param password
	 *            The password searched
	 * @return The LoginVO based on this combination
	 */
	UserVO getOne(final String username, final String password) throws ServiceException;

	/**
	 * Method to get a UserVO based on the userId
	 * 
	 * @param userId
	 *            The userId searched
	 * @return The UserVO whose this id belongs
	 * @throws ServiceException
	 *             if a DAOException has already been thrown
	 */
	UserVO getOneById(final int userId) throws ServiceException;

	/**
	 * Method to convert a UserDO into a UserVO
	 * 
	 * @param uDo
	 *            The UserDO to convert
	 * @return The UserVO resulted of this conversion
	 */
	UserVO getVOfromDO(final UserDO uDo);

	/**
	 * Method to convert a UserVO into a UserDO
	 * 
	 * @param uVo
	 *            The userVO to convert
	 * @return The UserDO resulted of this conversion
	 */
	UserDO getDOfromVO(final UserVO uVo);

	/**
	 * Method to convert the RegisterForm data into a UserDO
	 * 
	 * @param userData
	 *            The data of the register form
	 * @return A userDO based on these data
	 */
	UserDO getDOfromForm(final RegisterVO userData);

	/**
	 * Method to convert the RegisterForm data into a UserDO
	 * 
	 * @param userData
	 *            The data of the register form
	 * @return A userDO based on these data
	 */
	UserDO getDOfromForm(final ProfileVO userData);

	/**
	 * Method to add a new user
	 * 
	 * @param uVO
	 *            The new user to add
	 * @throws ServiceException
	 *             if a DAOException is already thrown
	 */
	void addOne(final RegisterVO user) throws ServiceException;

	/**
	 * Method to update a user
	 * 
	 * @param user
	 *            The user to update
	 * @throws ServiceException
	 *             if a DAOException is already thrown
	 */
	void update(final ProfileVO user) throws ServiceException;

}
