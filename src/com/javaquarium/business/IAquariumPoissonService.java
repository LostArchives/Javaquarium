package com.javaquarium.business;

import java.util.Map;

import com.javaquarium.beans.data.AquariumPoissonDO;
import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.exception.DAOException;
import com.javaquarium.exception.ServiceException;

/**
 * Classic service interface related to aquariumPoisson
 * 
 * @author Valentin
 *
 */
public interface IAquariumPoissonService {

	Map<Integer, AquariumPoissonVO> getAllAquariumPoisson(final int userId) throws ServiceException;

	AquariumPoissonVO getAquariumPoisson(final int userId, final int especeId) throws ServiceException, DAOException;

	AquariumPoissonVO getAquariumPoisson(final UserVO user, final PoissonVO espece) throws ServiceException;

	void addNewAquariumPoisson(final int userId, final int especeId, final int quantity) throws ServiceException;

	void addNewAquariumPoisson(final UserVO user, final PoissonVO espece, final int quantity) throws ServiceException;

	void updateAquariumPoisson(final AquariumPoissonDO aPdo) throws ServiceException;

	void removeAquariumPoisson(final AquariumPoissonDO aPdo) throws ServiceException;

	int getTotalAquariumPoisson(final int userId) throws ServiceException;

	void removeAllAquariumPoisson(final int userId) throws ServiceException;

	AquariumPoissonVO getVOfromDO(final AquariumPoissonDO apDO);

	AquariumPoissonDO getDOfromVO(final AquariumPoissonVO apVO, final UserVO uVo);

}
