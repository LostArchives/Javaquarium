package com.javaquarium.util;

import java.util.Date;

import com.javaquarium.business.IAquariumPoissonService;
import com.javaquarium.business.IPoissonService;
import com.javaquarium.business.IUserService;

/**
 * Class used as a catalogue for all services It will return an instance
 * (created by Spring) of the service requested
 * 
 * @author Valentin
 *
 */
public class CatalogueService {

	/**
	 * Instance of the PoissonService
	 */
	private IPoissonService poissonService;

	/**
	 * Instance of the userService
	 */
	private IUserService userService;

	/**
	 * Instance of the aquariumPoissonService
	 */
	private IAquariumPoissonService aquariumPoissonService;

	/**
	 * @return the poissonService
	 */
	public IPoissonService getPoissonService() {
		System.out.println("[" + new Date() + "] : Le service Poisson est demand�");
		return this.poissonService;
	}

	/**
	 * @param poissonService
	 *            the poissonService to set
	 */
	public void setPoissonService(final IPoissonService poissonService) {
		this.poissonService = poissonService;
	}

	/**
	 * @return the loginService
	 */
	public IUserService getUserService() {
		System.out.println("[" + new Date() + "] : Le service User est demand�");
		return this.userService;
	}

	/**
	 * @param loginService
	 *            the loginService to set
	 */
	public void setUserService(final IUserService loginService) {
		this.userService = loginService;
	}

	/**
	 * @return the aquariumPoissonService
	 */
	public IAquariumPoissonService getAquariumPoissonService() {
		System.out.println("[" + new Date() + "] : Le service AquariumPoisson est demand�");
		return this.aquariumPoissonService;
	}

	/**
	 * @param aquariumPoissonService
	 *            the aquariumPoissonService to set
	 */
	public void setAquariumPoissonService(final IAquariumPoissonService aquariumPoissonService) {
		this.aquariumPoissonService = aquariumPoissonService;
	}

}
