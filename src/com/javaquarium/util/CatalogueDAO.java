package com.javaquarium.util;

import java.util.Date;

import com.javaquarium.dao.IAquariumPoissonDAO;
import com.javaquarium.dao.IPoissonDAO;
import com.javaquarium.dao.IUserDAO;

/**
 * Class used as a catalogue for all DAOs It will return an instance of the DAO
 * (created by Spring) requested
 * 
 * @author Valentin
 *
 */
public class CatalogueDAO {

	/**
	 * Instance of the poissonDAO
	 */
	private IPoissonDAO poissonDAO;

	/**
	 * Instance of the userDAO
	 */
	private IUserDAO userDAO;

	/**
	 * Instance of the aquariumPoissonDAO
	 */
	private IAquariumPoissonDAO aquariumPoissonDAO;

	/**
	 * @return the poissonDAO
	 */
	public IPoissonDAO getPoissonDAO() {
		System.out.println("[" + new Date() + "] : Le DAO Poisson est demand�");
		return this.poissonDAO;
	}

	/**
	 * @param poissonDAO
	 *            the poissonDAO to set
	 */
	public void setPoissonDAO(final IPoissonDAO poissonDAO) {
		this.poissonDAO = poissonDAO;
	}

	/**
	 * @return the loginDAO
	 */
	public IUserDAO getUserDAO() {
		System.out.println("[" + new Date() + "] : Le DAO User est demand�");
		return this.userDAO;
	}

	/**
	 * @param userDAO
	 *            the userDAO to set
	 */
	public void setUserDAO(final IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	/**
	 * @return the aquariumPoissonDAO
	 */
	public IAquariumPoissonDAO getAquariumPoissonDAO() {
		System.out.println("[" + new Date() + "] : Le DAO AquariumPoisson est demand�");
		return this.aquariumPoissonDAO;
	}

	/**
	 * @param aquariumPoissonDAO
	 *            the aquariumPoissonDAO to set
	 */
	public void setAquariumPoissonDAO(final IAquariumPoissonDAO aquariumPoissonDAO) {
		this.aquariumPoissonDAO = aquariumPoissonDAO;
	}

}
